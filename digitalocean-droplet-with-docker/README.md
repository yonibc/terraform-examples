# Terraform DigitalOcean Example

## How To Use

Prepare configuration variables:

    cp _terraform.tfvars terraform.tfvars

Edit `terraform.tfvars` to suit your needs. You need an API access token:
https://cloudsupport.digitalocean.com/s/#none|ka21N000000Cp7lQAC and you need
to create and upload an SSH public key: https://cloudsupport.digitalocean.com/s/#none|ka21N000000Cp7SQAS

Use authentication with Terraform:

    terraform init
    terraform plan -out plan
    terraform apply plan

Look for *public_ip* at the end of terraform's apply output.

Login:

    ssh testadmin@<PUBLIC_IP>
    docker run -d -p 80:80 nginx:alpine

In your browser: `http://PUBLIC_IP/`

Destroy:

    terraform destroy -force

## Further Reading

Terraform DigitalOcean Provider: https://www.terraform.io/docs/providers/do/index.html
