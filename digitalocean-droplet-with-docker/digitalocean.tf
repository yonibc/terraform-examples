variable "access_token" {
  description = "DigitalOcean access token (see DigitalOcean web console API section)"
}

variable "ssh_keys" {
  description = "List of ssh_key IDs or fingerprints"
  type        = "list"
}

variable "ssh_private_key_path" {
  description = "path to SSH private key PEM file"
}

variable "ssh_user" {
  default = "root"
}

provider "digitalocean" {
  token = "${var.access_token}"
}

resource "digitalocean_droplet" "test-sma" {
  image    = "debian-9-x64"
  name     = "test-host"
  region   = "fra1"
  size     = "s-1vcpu-1gb"
  ssh_keys = "${var.ssh_keys}"

  provisioner "file" {
    source      = "provisioning"
    destination = "/"

    connection {
      type        = "ssh"
      user        = "${var.ssh_user}"
      private_key = "${file("${var.ssh_private_key_path}")}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "bash /provisioning/docker.sh"
    ]

    connection {
      type        = "ssh"
      user        = "${var.ssh_user}"
      private_key = "${file("${var.ssh_private_key_path}")}"
    }
  }
}

output "pubilc_ip" {
  value = "${digitalocean_droplet.test-sma.ipv4_address}"
}
