#!/bin/bash

set -x

apt-get update
apt-get install -y curl

curl -Ls http://get.docker.com | sudo bash
systemctl enable --now docker
