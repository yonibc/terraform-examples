variable "access_token" {
  description = "DigitalOcean access token (see DigitalOcean web console API section)"
}

variable "ssh_keys" {
  description = "List of ssh_key IDs or fingerprints"
  type        = "list"
}

provider "digitalocean" {
  token = "${var.access_token}"
}

resource "digitalocean_droplet" "test-sma" {
  image    = "docker-18-04"
  name     = "test-host"
  region   = "fra1"
  size     = "s-1vcpu-1gb"
  ssh_keys = "${var.ssh_keys}"
}

output "pubilc_ip" {
  value = "${digitalocean_droplet.test-sma.ipv4_address}"
}
