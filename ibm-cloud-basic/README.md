# IBM Cloud Terraform Example

## How To Use

    cp _terraform.tfvars terraform.tfvars

See cloud.ibm.com -> Verwalten -> Zugriff (IAM -> Benutzer -> <EIGENER BENUTZER> -> API-Schlüssel)

Edit `terraform.tfvars` and add your access credentials:

    bluemix_api_key = "IBM Cloud-API-Schlüssel"
    softlayer_username = "API-Schlüsseldetails für klassische Infrastruktur - API-Benutzername"
    softlayer_api_key = "API-Schlüsseldetails für klassische Infrastruktur - API-Schlüssel"
    ssh_key = "CONTENTS OF YOUR id_rsa.pub FILE"

Setup IBM Cloud Terraform Provider:

see https://github.com/IBM-Cloud/terraform-provider-ibm and https://ibm-cloud.github.io/tf-ibm-docs/

Run:

    terraform init
    terraform plan -out plan
    terraform apply plan
