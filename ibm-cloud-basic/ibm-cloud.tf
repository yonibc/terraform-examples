variable "bluemix_api_key" {
}

variable "softlayer_username" {
}

variable "softlayer_api_key" {
}

variable "ssh_key" {
    description = "ssh public key to access vm_instance"
}

provider "ibm" {
    bluemix_api_key = "${var.bluemix_api_key}"
    softlayer_username = "${var.softlayer_username}"
    softlayer_api_key = "${var.softlayer_api_key}"
    region = "europe"
}

resource "ibm_compute_ssh_key" "test_key_1" {
    label = "test_key_1"
    public_key = "${var.ssh_key}"
}

resource "ibm_compute_vm_instance" "test_server" {
    hostname = "test-server.test.de"
    domain = "test.de"
    ssh_key_ids = ["${ibm_compute_ssh_key.test_key_1.id}"]
    os_reference_code = "DEBIAN_9_64"
    datacenter = "fra02"
    network_speed = 10
    cores = 1
    memory = 1024
}
