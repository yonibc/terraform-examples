# VMware vSphere with Photon OS base VM/template Example

https://www.terraform.io/docs/providers/vsphere/index.html

Setup with vCenter since API write access is needed. Free license ESXi installations do not work.

## How to use

1. Download current Photon OS OVA: https://vmware.github.io/photon/ (can be deployed directly with download URL)
2. Deploy OVA into your vSphere infrastructure (https://docs.vmware.com/en/VMware-vSphere/6.5/com.vmware.vsphere.vm_admin.doc/GUID-17BEDA21-43F6-41F4-8FB2-E01D275FE9B4.html)
   1. example uses "photon-ova" as template name, name yours accordingly or change terraform config
   2. it es best to change the default root password of the resulting VM (https://github.com/vmware/photon/wiki/Running-Photon-OS-on-vSphere)
   3. disable password change policy

                passwd --maxdays -1 --warndays -1 --mindays -1 root
                tdnf check-update
                tdnf -y upgrade
                tdnf install kbd
                localectl set-keymap de-latin1

   4. eventually import a public SSH key into /root/.ssh/authorized_keys
   for later remote-exec provisioning

                cat .ssh/id_rsa.pub | ssh root@ADDRESS 'cat >> .ssh/authorized_keys'

3. Change names of vSphere data sources to match your infrastructure
4. copy variables template file:

        cp _terraform.tfvars terraform.tfvars

5. add your vSphere server's hostname and credentials
6. Run:

        terraform init
        terraform plan -out plan
        terraform apply plan
