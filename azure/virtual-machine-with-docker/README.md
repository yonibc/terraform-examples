# Terraform Azure Example

(Tested with Visual Studio Enterprise MPN account but any account type should work.)

## How To Use

Prepare configuration variables:

    cp _terraform.tfvars terraform.tfvars

Edit `terraform.tfvars` to suit your needs.

Authenticate to Azure with Azure CLI:

    az cloud set --name AzureCloud  # this is the default but to be sure ...
    az login

Use authentication with Terraform:

    terraform init
    terraform plan -out plan
    terraform apply plan

If output of public_ip is empty after apply try `terraform refresh`.

Login:

    ssh testadmin@<PUBLIC_IP>
    docker run -d -p 80:80 nginx:alpine

In your browser: `http://PUBLIC_IP/`

Destroy _(can take some time - sometimes > 10 minutes)_:

    terraform destroy -force

## Further Reading

Terraform Azure Provider: https://www.terraform.io/docs/providers/azurerm/index.html

Authenticating using Azure CLI: https://www.terraform.io/docs/providers/azurerm/auth/azure_cli.html
