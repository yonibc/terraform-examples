#!/bin/bash

set -x

username="$1"

sudo apt-get update
sudo apt-get install -y curl

curl -Ls http://get.docker.com | sudo bash
sudo systemctl enable --now docker

adduser "$username" docker
