provider "azurerm" {
  version = "=1.22.1"
}

variable "prefix" {
  description = "for namespacing confiugration items"
}

variable "location" {
  description = "location for this setup"
}

variable "admin_username" {
  default = "testadmin"
}

variable "ssh_key_path" {
  default = "~/.ssh/id_rsa"
}

variable "ssh_public_key_path" {
  default = "~/.ssh/id_rsa.pub"
}

resource "azurerm_resource_group" "main" {
  name     = "${var.prefix}-terraform"
  location = "northeurope"
}

resource "azurerm_virtual_network" "network" {
  name                = "${var.prefix}-network"
  address_space       = ["10.0.0.0/16"]
  location            = "${azurerm_resource_group.main.location}"
  resource_group_name = "${azurerm_resource_group.main.name}"
}

resource "azurerm_subnet" "subnet" {
  name                 = "${var.prefix}-subnet"
  address_prefix       = "10.0.1.0/24"
  resource_group_name  = "${azurerm_resource_group.main.name}"
  virtual_network_name = "${azurerm_virtual_network.network.name}"
}

resource "azurerm_public_ip" "public_host_ip" {
  name                    = "${var.prefix}-public-ip"
  location                = "${azurerm_resource_group.main.location}"
  resource_group_name     = "${azurerm_resource_group.main.name}"
  allocation_method       = "Dynamic"
  idle_timeout_in_minutes = 30

  tags {
    controller = "terraform"
  }
}

resource "azurerm_network_interface" "nic" {
  name                = "${var.prefix}-nic"
  location            = "${azurerm_resource_group.main.location}"
  resource_group_name = "${azurerm_resource_group.main.name}"

  ip_configuration {
    name                          = "ip1"
    subnet_id                     = "${azurerm_subnet.subnet.id}"
    private_ip_address_allocation = "Dynamic"
    public_ip_address_id          = "${azurerm_public_ip.public_host_ip.id}"
  }
}

resource "azurerm_virtual_machine" "host" {
  name                  = "${var.prefix}-vm"
  location              = "${azurerm_resource_group.main.location}"
  resource_group_name   = "${azurerm_resource_group.main.name}"
  network_interface_ids = ["${azurerm_network_interface.nic.id}"]
  vm_size               = "Standard_B1s"

  delete_os_disk_on_termination    = true
  delete_data_disks_on_termination = true

  storage_image_reference {
    publisher = "Canonical"
    offer     = "UbuntuServer"
    sku       = "18.04-LTS"
    version   = "latest"
  }

  storage_os_disk {
    name              = "${var.prefix}-os-disk"
    caching           = "ReadWrite"
    create_option     = "FromImage"
    managed_disk_type = "Standard_LRS"
  }

  os_profile {
    computer_name  = "testhost"
    admin_username = "${var.admin_username}"
  }

  os_profile_linux_config {
    disable_password_authentication = true

    ssh_keys {
      path     = "/home/${var.admin_username}/.ssh/authorized_keys"
      key_data = "${file("${var.ssh_public_key_path}")}"
    }
  }

  tags {
    controller = "terraform"
  }

  provisioner "file" {
    source      = "provisioning"
    destination = "/home/${var.admin_username}"

    connection {
      type        = "ssh"
      user        = "${var.admin_username}"
      private_key = "${file("${var.ssh_key_path}")}"
    }
  }

  provisioner "remote-exec" {
    inline = [
      "id",
      "sudo bash /home/${var.admin_username}/provisioning/docker.sh ${var.admin_username}",
    ]

    connection {
      type        = "ssh"
      user        = "${var.admin_username}"
      private_key = "${file("${var.ssh_key_path}")}"
    }
  }
}

data "azurerm_public_ip" "ip" {
  name                = "${azurerm_public_ip.public_host_ip.name}"
  resource_group_name = "${azurerm_resource_group.main.name}"
}

output "ip_address" {
  value = "${data.azurerm_public_ip.ip.ip_address}"
}
